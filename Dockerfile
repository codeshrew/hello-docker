FROM nginx:1.9.7
MAINTAINER Stefan Liedle <stefan.liedle@gmail.com>

COPY . /usr/share/nginx/html

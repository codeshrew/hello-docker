This is a basic example Docker set up.

Run with `docker-compose up` or `docker-compose up -d` to put it in the background.

# Install Docker
If you don't have Docker installed yet, download and run the [Docker Toolbox installer](https://www.docker.com/docker-toolbox).
